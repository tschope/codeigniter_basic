<!DOCTYPE HTML>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="tschope/WT" />

	<title>Novo Cadastro Cobertor Social</title>
    <style>
        
        body{width: 550px; font-family: helvetica;}
    
    </style>
    
</head>

<body>
    <h1>
        Novo Cadastro
    </h1>
    
    <p>Tivemos um novo cadastro no site. Segue os dados do usuário:</p>
    <?php
        if(!empty($dados['avatar']))
        { ?>
            <p><img src="<?php echo base_url(); ?>userfiles/<?php echo $dados['avatar']; ?>" /></p>
     <? }
        else
        { ?>
            <p><img src="<?php echo static_url(); ?>images/photo-load.png" /></p>
     <? }
    ?>
    <p><strong>Nome:</strong> <?php echo $dados['name']; ?><br />
       <strong>Valor doado:</strong> R$ <?php echo number_format($dados['pay_value'], 2, ',', '.'); ?></p>
    <p>&nbsp;</p>
    <p style="font-size: 10px;">Esse e-mail foi enviado automaticamente pelo sistema de Cobertor Social.
       O cadastro não garante a doação ou inserção automática de dados no site, servindo apenas para controle de novos usuários.
    </p>
    <p><img src="<?php echo static_url(); ?>images/grupo_newcom.jpg" /></p>
</body>
</html>