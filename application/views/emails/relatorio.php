<!DOCTYPE HTML>
<head>
	<meta http-equiv="content-type" content="text/html" />
	<meta name="author" content="tschope/WT" />

	<title>Novo Cadastro Cobertor Social</title>
    <style>
        
        body{width: 550px; font-family: helvetica;}
    
    </style>
    
</head>

<body>
    <h1>Relátorio Cobertor Social</h1>

    <p>Segue relátorio de arrecadação do Projeto:</p>
    <p>
       <strong>Total de Retalhos: </strong> <?php echo $dados['TotalRetalhos']; ?> <br />
       <strong>Total de Retalhos Disponíveis: </strong> <?php echo $dados['TotalRetalhosDisponiveis']; ?> <br />
       <strong>Total de Retalhos Pagos: </strong> <?php echo $dados['totalRetalhosComprados']; ?> <br />
       <strong>Total de Retalhos Reservados: </strong> <?php echo $dados['totalRetalhosReservados']; ?> <br />
       <strong>Total de Pagamentos Confirmados: </strong> R$<?php echo $dados['valorArrecadado']; ?> <br />
       <strong>Total de Pagamentos Pendentes: </strong> R$<?php if(!empty($dados['valorPendente'])){echo $dados['valorPendente'];}else{echo '0.00';}; ?> <br />
       <strong>Total de Pagamentos: </strong> R$<?php echo $dados['totalQPodeSerArrecadado']; ?> 
    </p>
    <p>&nbsp;</p>
    <p style="font-size: 10px;">Esse e-mail foi enviado automaticamente pelo sistema de Cobertor Social.
       Ele é gerado de 2 em 2 dias as 3h e pode mudar a qualquer momento;
    </p>
    <p><img src="<?php echo static_url(); ?>images/grupo_newcom.jpg" /></p>
</body>
</html>