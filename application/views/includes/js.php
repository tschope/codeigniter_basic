  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo static_url(); ?>js/libs/jquery-1.7.2.min.js"><\/script>')</script>
  
  <!-- load scripts libraries -->
  <?php echo $this->dynamicload->loadFiles("footer"); ?>
  <!-- end scripts libraries -->
  
  <!-- scripts concatenated and minified via build script -->
  <script src="<?php echo static_url(); ?>js/plugins.js"></script>
  <script src="<?php echo static_url(); ?>js/script.js"></script>
  <!-- end scripts -->