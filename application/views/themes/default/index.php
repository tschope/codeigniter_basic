<?php include("header.php")?>
    
    <h1>Welcome to CodeIgniter!</h1>
    
    <div id="body" role="main">
    
        <p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

		<p>If you would like to edit this page you'll find it located at:</p>
		<code>application/views/themes/<?php echo $theme_name; ?>/welcome_message.php</code>

		<p>The corresponding controller for this page is found at:</p>
		<code>application/controllers/welcome.php</code>

		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>
        
        <p>This default theme, views and statics files, is from Boilerplate. A rock-solid default for HTML5 awesome! Enjoy on:</p>
        <code>http://html5boilerplate.com/</code>
          
    </div>
    
    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>

<?php include("footer.php")?>