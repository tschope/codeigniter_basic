<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/b/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><?php echo $title_page; ?></title>
  <meta name="description" content="">
  <meta name="author" content="Rodrigo G Tschope">

  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
  <link href="<?php echo $theme_url; ?>favicon.ico" rel="icon" type="image/x-icon" />

  <!-- CSS: implied media=all -->
  <!-- CSS concatenated and minified via ant build script-->
  <link rel="stylesheet" href="<?php echo $theme_url; ?>css/style.css">
  <!-- end CSS-->

  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

  <!-- All JavaScript at the bottom, except for Modernizr / Respond.
       Modernizr enables HTML5 elements & feature detects; Respond is a polyfill for min/max-width CSS3 Media Queries
       For optimal performance, use a custom Modernizr build: www.modernizr.com/download/ -->
  <script src="<?php echo $theme_url; ?>js/libs/modernizr-2.0.6.min.js"></script>
  <style>
	  body { text-align: center;}
	  h1 { font-size: 50px; text-align: center }
	  span[frown] { transform: rotate(90deg); display:inline-block; color: #bbb; }
	  body { font: 20px Constantia, 'Hoefler Text',  "Adobe Caslon Pro", Baskerville, Georgia, Times, serif; color: #999; text-shadow: 2px 2px 2px rgba(200, 200, 200, 0.5); }
	  ::-moz-selection{ background:#000; color:#fff; }
	  ::selection { background:#000; color:#fff; } 
	  article {display:block; text-align: left; width: 500px; margin: 0 auto; }
	  
	  a { color: rgb(36, 109, 56); text-decoration:none; }
	  a:hover { color: rgb(96, 73, 141) ; text-shadow: 2px 2px 2px rgba(36, 109, 56, 0.5); }
  </style>
</head>
<body>
     <article>
	  <h1>Not found <span frown>:(</span></h1>
	   <div>
	       <p>Sorry, but the page you were trying to view does not exist.</p>
	       <p>It looks like this was the result of either:</p>
	       <ul>
		   <li>a mistyped address</li>
		   <li>an out-of-date link</li>
	       </ul>
	   </div>
     </article>
     
     <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
<!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline 
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo $theme_url; ?>js/libs/jquery-1.7.1.min.js"><\/script>')</script>-->


  <!-- scripts concatenated and minified via ant build script
  <script defer src="<?php echo $theme_url; ?>js/plugins.js"></script>
  <script defer src="<?php echo $theme_url; ?>js/script.js"></script>
   end scripts-->

	
  <!-- Change UA-XXXXX-X to be your site's ID 
  <script>
    window._gaq = [['_setAccount','UAXXXXXXXX1'],['_trackPageview'],['_trackPageLoadTime']];
    Modernizr.load({
      load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
    });
  </script>-->


  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7 ]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->
  
</body>
</html>