<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedido extends Public_Controller {
    
    
     /**
     * Descricao dos IDs Status
     * 
     * STATUS DE PAGAMENTO
     * 
     * 0 = Pedido Feito
     * 1 = Aprovado;
     * 2 = Em Analise
     * 3 = Aguardando Pagamento
     * 4 = Completado
     * 5 = Cancelado
     * 
     * STATUS DO USER
     * 
     * 0 = Cancelado ou Anulado
     * 1 = Aprovadp
     * 2 = Reservado
     * 3 = Anonimo(doação feita porem Usuario Moderado
     * 
     * STATUS PAGSEGURO
     * 
     * 1 = Aguardando 
     * 2 = Em análise
     * 3 = Paga
     * 4 = Disponível
     * 5 = Em disputa
     * 6 = Devolvida
     * 7 = Cancelada
     *  
     */
    //All Data
    var $data = null;
    var $dataDebugging = null;
   
   /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // Adicionando um produto
        $this->load->library('pgs_pgs');
        
        $this->data['pgs_emailcobranca'] =  $this->pgs_pgs->getemail();	
        $this->data['pgs_token'] =  $this->pgs_pgs->gettoken();

    }

   /**
     * Privates Functions
     */
    private function _trataMoney($money)
    {
        $money = str_replace(".", "", $money);
        $money = str_replace(",", ".", $money);
        return $money;
    }
    
      
   /**
    * Aqui dispara o email de notificação para um novo cadastro na plataforma 
    * 
    * @param $emails String or Array
    * @return boolean  
    */
    private function sendNotification($emails,$data)
    {
        
        $email_title = 'Nova doação realizada';
        
        $this->data['dados'] = $data;
        
        $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
               );
         $this->load->library('email', $config);

         $this->email->from('cobertorsocial@cobertorsocial.com.br', utf8_encode('Cobertor Social'));
         $this->email->to($emails);
         
         //Carrega a view e coloca na view dados como email e etc;
         $body = $this->load->view("emails/novo-cadastro", $this->data, TRUE);
          
         $this->email->subject($email_title);
         $this->email->message($body);

         if (!$this->email->send())
         {
            // Generate error
            return false;
         }
         else
         {
            return true;
         }
        
    }

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */   
    public function index()
	{
           
        /*Teste do FireBug + FirePHP
        $this->dataDebugging['array'] = array (
          'language' => 'PHP',
          'database' => 'MySQL',
          'blogging platform' => 'WordPress',
          'post' => 'CodeIgniter and FirePHP',
        );
        
        $this->dataDebugging['info'] = "information";
        $this->dataDebugging['warn'] = "this is a warning";
        $this->dataDebugging['error'] = "error message";
        
        //Message LOG on FirePHP
        $this->firephp->log($this->dataDebugging['array']);
        //Message INFO on FirePHP
        $this->firephp->info($this->dataDebugging['info']);
        //Message Warning on FirePHP
        $this->firephp->warn($this->dataDebugging['warn']);
        //Message Error on FirePHP
        $this->firephp->error($this->dataDebugging['error']);
        */
        
        $this->load->model('Facebook_model');
        $this->load->library('Facebook');
        
        $fb_data = $this->session->userdata('fb_data'); // This array contains all the user FB information
 
        if((!$fb_data['uid']) or (!$fb_data['me']))
        {
            // If this is a protected section that needs user authentication
            // you can redirect the user somewhere else
            // or take any other action you need
            $this->data['fb_data'] = $fb_data;
        }
        else
        {
            
            //var_dump($fb_data);

            $this->data['fb_data'] = $fb_data;
        
        }
        
        $this->load->view('pagseguro', $this->data);
		
	}
    
    public function processa()
    {
        $this->load->libraries('form_validation');
        
        $rules = array(
                    array(
                            'field' => 'name-CPF',
                            'label' => 'required',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'name-CNPJ',
                            'label' => 'required',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'facebook_id',
                            'label' => '',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'email',
                            'label' => '',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'avatar',
                            'label' => '',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'pay_value',
                            'label' => '',
                            'rules' => 'required|xss_clean'
                         ),
                    array(
                            'field' => 'position',
                            'label' => '',
                            'rules' => 'required|xss_clean'
                         ),
                    array(
                            'field' => 'color_id',
                            'label' => '',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'type',
                            'label' => '',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'document-CPF',
                            'label' => '',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'document-CNPJ',
                            'label' => '',
                            'rules' => 'xss_clean'
                         )
                    );
        
        $validate = $this->form_validation->validate_data($rules);
        $data_validate = $validate['data'];
        
        //Valida campos
        if ($validate['status'] == '1')
        {

            $this->data['error'] = null;
            
            if(empty($this->data['error']))
            {
                
                if($data_validate['type'] == 'PJ')
                {
                    $data_validate['name'] = $data_validate['name-CNPJ'];
                    $data_validate['document'] = $data_validate['document-CNPJ'];
                }
                elseif($data_validate['type'] == 'PF')
                {
                    $data_validate['name'] = $data_validate['name-CPF'];
                    $data_validate['document'] = $data_validate['document-CPF'];
                }
                else
                {
                    $data_validate['name'] = $data_validate['name-CPF'];
                    $data_validate['document'] = $data_validate['document-CPF'];
                }

                
                $data_validate['position'] = $this->_verify_position($data_validate['position']);
                
                $u = new users_model();
                
                $u->facebook_id                = $data_validate['facebook_id'];
                $u->name                       = $data_validate['name'];
                $u->email                      = $data_validate['email'];
                $u->avatar                     = $data_validate['avatar'];
                $u->pay_value                  = $this->_trataMoney($data_validate['pay_value']);
                $u->transaction_id             = null;
                $u->status_payment             = 0; //0 = Pedido Feito
                $u->status                     = 0; //2 = Reservado
                $u->position                   = $data_validate['position'];
                $u->color_id                   = $data_validate['color_id'];
                $u->type                       = $data_validate['type'];
                $u->document                   = $data_validate['document'];
                $u->term_accept                = 1;
                $u->date_status_change         = date('Y-m-d H:i:s');
                
                $u->save();
                
                $user_id = $u->id;
                
                $dadosEmail = array();
                $dadosEmail['name'] = $data_validate['name'];
                if(!empty($data_validate['avatar']))
                {
                    if(!empty($data_validate['facebook_id']))
                    {
                        $dadosEmail['avatar'] = 'facebook/thumbnails/'.$data_validate['avatar'];
                    }
                    else
                    {
                        $dadosEmail['avatar'] = 'thumbnails/'.$data_validate['avatar'];
                    }
                }
                $dadosEmail['pay_value'] = $this->_trataMoney($data_validate['pay_value']);
                
                $emailDestiny = array('tschope@gmail.com','fabiodiasbh@gmail.com','rh@gruponewcomm.com.br','douglas.miguel@gmail.com');
                
                $this->sendNotification($emailDestiny, $dadosEmail);

                $url = "https://ws.pagseguro.uol.com.br/v2/checkout";
                
                $data['email'] = $this->data['pgs_emailcobranca'];
                $data['token'] = $this->data['pgs_token'];
                $data['currency'] = 'BRL';
                $data['itemId1'] = '1';
                $data['itemDescription1'] = 'Cobertor Social';
                $data['itemAmount1'] = $this->_trataMoney($data_validate['pay_value']); //Recebimento por Post
                $data['itemQuantity1'] = '1';
                $data['itemWeight1'] = '1';
                $data['itemShippingCost1'] = '0.00';

                $data['encoding'] = 'UTF-8';

                $data['reference'] = $user_id;
                
                $explodeName = explode(' ', $data_validate['name']);
                if(count($explodeName)>1)
                    $data['senderName'] = (string)$data_validate['name']; // Nome pega por post ou FB Session
                
                if(!empty($data_validate['email']))    
                    $data['senderEmail'] = (string)$data_validate['email']; // Nome pega por post ou FB Session
                
                //$data['redirectURL'] = base_url().'pagseguro/pedido/concluido/';
                $data['redirectURL'] = 'http://www.cobertorsocial.com.br/pagseguro/pedido/concluido/';
                
                $data = http_build_query($data);
                
                $curl = curl_init($url);
                
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                $xml= curl_exec($curl);
                
                if($xml == 'Unauthorized'){
                    //Insira seu código de prevenção a erros
                    $this->data['error'] = 'Seu pedido não foi autorizado';
                    die($this->data['error']);
                    //header('Location: erro.php?tipo=autenticacao');
                    //exit;//Mantenha essa linha
                }
                curl_close($curl);
                
                $xml= simplexml_load_string($xml);
                if(count($xml->error) > 0){
                     //Insira seu código de tratamento de erro, talvez seja útil enviar os códigos de erros.
                    $this->data['error'] = 'Dados Invalidos: '.$xml->error;
                    die($this->data['error']);
                    //die('dados invalidos');
                     //header('Location: erro.php?tipo=dadosInvalidos');
                     //exit;
                }
                
                $this->output->set_header('Location: https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml->code);
            

            }
            
            //
            //header('Location: https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $xml -> code);
            
            
        }
        else
        {
            //ERROR
            die(var_dump($validate));
        }
        
        
    }
    
    public function concluido()
    {
        
        if(isset($_GET['transaction_id']))
        {
        
            $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/'.$_GET['transaction_id'].'?email='.$this->data['pgs_emailcobranca'].'&token='.$this->data['pgs_token'];
            
            $xml = simplexml_load_file($url);
            
            if($xml == 'Not Found')
            {
                echo 'transition_id invalidaded';
            }
            else
            {
                $user_id         = $xml->reference;
                $status_pedido   = $xml->status;
                $transaction_id  = $xml->code;
                
                //td ocorreu como uma luva. Siga em frente!
                $u = new users_model();
                
                $u->get_by_id($user_id);
                
                if($u->exists())
                {
                    
                    $tipo = $u->type;

                    $u->clear();
                    
                    $u->id                         = $user_id;
                    $u->transaction_id             = (string)$transaction_id;
                    $u->status_payment             = $status_pedido;
                    if($status_pedido == 3)
                    {
                        if($tipo == 'PJ')
                        {
                          $u->status                 = 4;  
                        }
                        else
                        {
                          $u->status                 = 1;  
                        }
                    }
                    $u->date_status_change         = date('Y-m-d H:i:s');
                    
                    $u->save();
                    
                    $user_id = $u->id;
                    
                    redirect('/front/obrigado/');
                    
                }
                else
                {
                    
                    echo 'usuario nao cadastrado';
                    
                }
                
            }
            
        }
        else
        {
            $this->output->set_header('Location: '.base_url());
        }
               
        
    }
    
    /** verify position **
    * verify if url is available
    * @access private
    * @return string
    */
    private function _verify_position($position, $number = NULL, $newNumber = NULL)
    { 
        $u = new users_model();
        $u->where(array('position' => $position, 'status >' => 0))->get();
        $number = $number == NULL ? 0 : $number;
        
        if ($u->exists())
        {
            $number += 1;
            
            if (empty($newNumber))
            {
                $newNumber = $position;
                $position = $position+$number;
            }
            else
            {
                $position = $newNumber+$number;
            }
            
            return $this->_verify_position($position,$number,$newNumber);
        }
        else
        {
            return $position;
        }
    }
 
}


?>