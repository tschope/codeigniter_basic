<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Retorno extends Public_Controller {
    
    //All Data
    var $data = null;
    var $dataDebugging = null;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();	
        
        // Adicionando um produto
        $this->load->library('pgs_pgs');
        
        $this->data['pgs_emailcobranca'] =  $this->pgs_pgs->getemail();	
        $this->data['pgs_token'] =  $this->pgs_pgs->gettoken();	

    }
    
    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */   
    public function index(){

        $query_string = "";
        if ($_POST) {
          $kv = array();
          foreach ($_POST as $key => $value) {
            $kv[] = "$key=$value";
          }
          $query_string = join("&", $kv);
        }
        else {
          $query_string = $_SERVER['QUERY_STRING'];
        }
        
        $query_string .= " - ".date('Y-m-d H:i:s');
        $query_string .= "\n";
        
        $this->load->helper('file');
        
        if (!write_file('./userfiles/logs/access.pagseguro.log', $query_string, 'a+'))
        {
             echo 'Unable to write the file';
        }
        else
        {
             echo 'File written!';
        }
        
        if(isset($_POST['notificationCode']))
        {
            $notificationCode = $_POST['notificationCode'];

            $result = $this->ckechTransition($notificationCode);
            
            if($result)
            {
                 echo 'Atualizado';
            }
            else
            {
                echo 'ocorreu algum problema durante a atualização';
            }  
        }
        else
        {
            echo 'Voce nao deveria estar aqui';
        }

    }
    
    private function ckechTransition($notificationCode)
    {

        $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/'.$notificationCode.'?email='.$this->data['pgs_emailcobranca'].'&token='.$this->data['pgs_token'];

        $xml = simplexml_load_file($url);

        if($xml == 'Not Found')
        {
            return false;
        }
        else
        {
            $user_id         = $xml->reference;
            $status_pedido   = $xml->status;
            $transaction_id  = $xml->code;
            
            $senderEmail = (string)$xml->sender->email;
            
            //td ocorreu como uma luva. Siga em frente!
            $u = new users_model();
            
            $u->get_by_id($user_id);
            
            if($u->exists())
            {
                $tipo = $u->type;
                
                $u->clear();
                
                $u->id                         = $user_id;
                $u->transaction_id             = (string)$transaction_id;
                $u->status_payment             = $status_pedido;
                if($status_pedido == 1 OR $status_pedido == 2)
                {
                    $u->status                 = 2;
                }
                elseif($status_pedido == 3)
                {
                    if($tipo == "PJ")
                        $u->status                 = 4;
                    else
                        $u->status                 = 1;
                }
                elseif($status_pedido == 4)
                {
                    $u->status                 = 0;
                }
                elseif($status_pedido >= 5)
                {
                    $u->status                 = 0;
                }
                
                $u->email                      = $senderEmail;
                $u->date_status_change         = date('Y-m-d H:i:s');
                
                $u->save();
                
                $user_id = $u->id;
                
                return true;
                
            }
            else
            {
                
                return false;
                
            }
            
        }
            
    }


}


?>