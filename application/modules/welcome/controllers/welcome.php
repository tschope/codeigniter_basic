<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */   
    public function index()
	{
           
        //Teste do FireBug + FirePHP
        $this->dataDebugging['array'] = array (
          'language' => 'PHP',
          'database' => 'MySQL',
          'blogging platform' => 'WordPress',
          'post' => 'CodeIgniter and FirePHP',
        );
        
        $this->dataDebugging['info'] = "information";
        $this->dataDebugging['warn'] = "this is a warning";
        $this->dataDebugging['error'] = "error message";
        
        //Message LOG on FirePHP
        $this->firephp->log($this->dataDebugging['array']);
        //Message INFO on FirePHP
        $this->firephp->info($this->dataDebugging['info']);
        //Message Warning on FirePHP
        $this->firephp->warn($this->dataDebugging['warn']);
        //Message Error on FirePHP
        $this->firephp->error($this->dataDebugging['error']);

        $this->dynamicload->addCSS(array(
                                       'href' => 'uploadify.css',
                                       'rel'  => 'stylesheet', // default is "stylesheet"
                                       'type' => 'text/css'
                                       )
                                   );
        
        $this->dynamicload->addJS(array(
                                        'src'  => 'libs/jquery.uploadify-3.1.min.js',
                                        'type' => 'text/javascript'
                                        )
                                  );
        
        $this->data['title_page'] = 'Welcome to Codeigniter by Rodrigo Tschope';
        
        $this->load->view('index', $this->data);
		
	}
    
    public function facebook()
    {
        
        $this->load->model('Facebook_model');
        $this->load->library('Facebook');
        
        $fb_data = $this->session->userdata('fb_data'); // This array contains all the user FB information
 
        if((!$fb_data['uid']) or (!$fb_data['me']))
        {
            // If this is a protected section that needs user authentication
            // you can redirect the user somewhere else
            // or take any other action you need
            $this->data['fb_data'] = $fb_data;
            $this->load->view('facebook', $this->data);
        }
        else
        {
            
            //var_dump($fb_data);

            $this->data['fb_data'] = $fb_data;
            $this->load->view('facebook', $this->data);
        
        }
        
        
    }
    
    public function phpinfo()
    {
        die(phpinfo());
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */