<?php $this->load->view('includes/header');?>

<div class="main pagesize"> <!-- *** mainpage layout *** -->
  
  <h1>Cadastros</h1>

  <!-- CONTENT BOX - DATATABLE -->
      <div class="content-box">
      <div class="box-body">
        
        <div class="box-wrap clear">
            <p> Legendas:</p>
            <p>
            <?php 
                echo '<img alt="" title="active" class="icon16" src="'.ad_static_url().'img/ico_active_16.png"> = Aprovado ';
                echo '<img alt="" title="inactive" class="icon16" src="'.ad_static_url().'img/ball_yellow_16.png"> = Reservado ';
                echo '<img alt="" title="inactive" class="icon16" src="'.ad_static_url().'img/ball_purple_16.png"> = Anônimo ';
                echo '<img alt="" title="inactive" class="icon16" src="'.ad_static_url().'img/ball_blue_16.png"> = Empresa ';
                echo '<img alt="" title="inactive" class="icon16" src="'.ad_static_url().'img/ico_inactive_16.png"> = Reprovado ';
                ?>
            </p>
          <!-- TABLE -->
          <div id="data-table">
          
            <form method="post" action="">
            
            <!--<div style='display:block;height:40px;'>
              <a class="button red fl-space" href="<?php echo base_url(); ?>tam/add/">Adicionar Oferta</a>
            </div>-->
            
            <table class="style1 datatable">
            <thead>
              <tr>
                <th>Posição</th>
                <th>Foto</th>
                <th>Nome</th>
                <th>Valor doado</th>
                <th>Status de pagamento</th>
                <th>Status</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
                <?php 
            foreach($flaps as $key => $flap) :
                    
                    if(!empty($flap->facebook_id))
                    {
                        $urlAvatar = base_url().'userfiles/facebook/thumbnails/'.$flap->avatar;
                    }
                    else
                    {
                        if(empty($flap->avatar))
                        {
                            $urlAvatar = static_url().'admin/img/avatar.jpg';
                        }
                        else
                        {
                            $urlAvatar = base_url().'userfiles/thumbnails/'.$flap->avatar;
                        }

                    }

                    ?>
                <tr>
                    <td><?php echo $flap->position; ?></td>
                    <td><img src="<?php echo $urlAvatar; ?>" /></td>
                    <td><?php echo $flap->name; ?></td>
                    <td><?php echo 'R$ '.number_format($flap->pay_value, 2, ',', '.'); ?></td>
                    <td><?php echo $flap->status_payment; ?></td>
                    <td><?php 
                    if($flap->status == 1)
                    {
                        echo '<img alt="" title="active" class="icon16" src="'.ad_static_url().'img/ico_active_16.png">';
                    }
                    elseif($flap->status == 2)
                    {
                        echo '<img alt="" title="inactive" class="icon16" src="'.ad_static_url().'img/ball_yellow_16.png">';
                    }
                    elseif($flap->status == 3)
                    {
                        echo '<img alt="" title="inactive" class="icon16" src="'.ad_static_url().'img/ball_purple_16.png">';
                    }
                    elseif($flap->status == 4)
                    {
                        echo '<img alt="" title="inactive" class="icon16" src="'.ad_static_url().'img/ball_blue_16.png">';
                    }
                    else
                    {
                         echo '<img alt="" title="inactive" class="icon16" src="'.ad_static_url().'img/ico_inactive_16.png">';
                    }
                    ?></td>
                    <td>
                      <a href="<?php echo base_url(); ?>admin/users/edit/<?php echo $flap->id; ?>/"><img src="<?php echo ad_static_url(); ?>img/ico_edit_16.png" class="icon16 fl-space2" alt="" title="Editar" /></a>
                    </td>
                </tr>

               <?php
                    
                    ?>
                </div>
                <?php
            endforeach;
               ?>                                                      
              
            </tbody>
            </table>
            
            <div class="tab-footer clear fl">
              
            </div>
            </form>
          </div><!-- /#table -->

        </div><!-- end of box-wrap -->
      </div> <!-- end of box-body -->
      </div> <!-- end of content-box -->

</div>

  <?php $this->load->view('includes/footer');?>

  <script type="text/javascript">
  
  $(document).ready(function () {

      //InitMisc ();
      
      InitCufon (); 

      //InitEvents ();

      //InitBoxSlide ();

      //InitGraphs ();
        
      //InitNotifications ();
    
      //InitContentBoxes ();
    
      InitTables ();  

      InitFancybox ();
     
      //InitWYSIWYG ();
          
      //InitQuickEdit ();
      
      //InitMenuEffects ();
      
  });

  </script>

</body>
</html>