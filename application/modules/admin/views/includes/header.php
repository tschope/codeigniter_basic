<!DOCTYPE html>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="description"  content=""/>
  <meta name="keywords" content=""/>
  <meta name="robots" content="ALL,FOLLOW"/>
  <meta name="Author" content="Wunderman - Rodrigo Tschope"/>
  <meta http-equiv="imagetoolbar" content="no"/>
  <title><?php echo $title_page; ?></title>

  <link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/reset.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/screen.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/jquery.wysiwyg.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/jquery.ui.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/visualize.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/visualize-light.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/fancybox.css" type="text/css"/>
  <!--[if IE 7]>
    <link rel="stylesheet" type="text/css" href="css/ie7.css" />
  <![endif]--> 
  <script src="<?php echo ad_static_url(); ?>js/libs/modernizr-2.5.3.min.js"></script> 
</head>

<body>

<div class="pagetop">
  <div class="head pagesize"> <!-- *** head layout *** -->
    <div class="head_top">
      <div class="topbuts">
        <ul class="clear">
        <li><a href="<?php echo base_url(); ?>admin/login/logout/" class="red">Logout</a></li>
        </ul>
        
        <div class="user clear">
          <img src="<?php echo ad_static_url(); ?>img/avatar.jpg" class="avatar" alt="" />
          <span class="user-detail">
            <span class="name">Welcome Wunderman</span>
            <span class="text">Logged as wunderman</span>
          </span>
        </div>
      </div>
      
      <div class="logo clear">
      <a href="<?php echo base_url(); ?>admin/index/" title="View dashboard">
        <img src="<?php echo ad_static_url(); ?>img/logoWT.gif" alt="" class="picture" />
        
      </a>
      </div>
    </div>
    
    <div class="menu">
      <ul class="clear">
      <li class="active"><a href="<?php echo base_url(); ?>admin/index/">Home</a></li>
      
      </ul>
    </div>
  </div>
</div>