  <div class="footer">
    <div class="pagesize clear">
      <p class="bt-space15"><span class="copy"><strong>© 2012 Copyright by <a href="http://www.wunderman.com.br">Wunderman Technologies.</a></strong></span> </p>
    </div>
  </div>

   <!-- DEFINICIONS -->
	<script>
		var base_url = '<?php echo base_url(); ?>';
		var absolute_path = '<?php echo absolute_path(); ?>';
		var ad_static_url = '<?php echo ad_static_url(); ?>';
	</script>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo ad_static_url(); ?>js/libs/jquery-1.7.1.min.js"><\/script>')</script>

  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.visualize.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.wysiwyg.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/tiny_mce/jquery.tinymce.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.idtabs.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.datatables.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.fancybox.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.jeditable.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.ui.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.jcarousel.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.validate.js"></script>

  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/excanvas.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/cufon.js"></script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/Zurich_Condensed_Lt_Bd.js"></script>

  <script src="<?php echo ad_static_url(); ?>js/plugins.js"></script>
  <script src="<?php echo ad_static_url(); ?>js/script.js"></script>
