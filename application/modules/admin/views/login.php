<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="description"  content=""/>
<meta name="keywords" content=""/>
<meta name="robots" content="ALL,FOLLOW"/>
<meta name="Author" content="AIT"/>
<meta http-equiv="imagetoolbar" content="no"/>
<title><?php echo $title_page; ?></title>

<link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/reset.css" type="text/css"/>
<link rel="stylesheet" href="<?php echo ad_static_url(); ?>css/screen.css" type="text/css"/>
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="css/ie7.css" />
<![endif]-->	
</head>

<body class="login">

<div class="login-box">
<div class="login-border">
<div class="login-style">
	<div class="login-header">
		<div class="logo clear">
			<span class="textlogo">
				<span class="title">ADMIN COBERTOR SOCIAL</span>
			</span>
		</div>
	</div>
	<form action="<?php echo base_url(); ?>admin/login/loginPost" method="post" id="login-form">
		<div class="login-inside">
			<div id="message_login" style="padding-left: 42px; width:450px; margin: 10px 30px;"></div>
			<div class="login-data">
				<div class="row clear">
					<label for="user">Login:</label>
    					<input type="text" value="" size="25" class="text" id="login" />
    				</div>
 				<div class="row clear">
					<label for="password">Senha:</label>
					<input type="password" value="" size="25" class="text" id="pass" />
				</div>
				<input type="submit" class="button" value="Entrar" />
			</div>
		</div>
	</form>

</div>
</div>
</div>

<div class="login-links">
	<p><strong>&copy; 2012 Copyright by <a href="http://www.wunderman.sk">Wunderman Technology.</a></strong> All rights reserved.</p> 
</div>

<!-- DEFINICIONS -->
	<script>
		var base_url = '<?php echo base_url(); ?>';
		var absolute_path = '<?php echo absolute_path(); ?>';
		var ad_static_url = '<?php echo ad_static_url(); ?>';
	</script>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="<?php echo ad_static_url(); ?>js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <script type="text/javascript" src="<?php echo ad_static_url(); ?>js/libs/jquery.ui.js"></script>

<script>
      $(document).ready(function()
		{
			// We'll catch form submission to do it in AJAX, but this works also with JS disabled
			$('#login-form').submit(function(event)
			{
				// Stop full page load
				event.preventDefault();
				
				// Check fields
				var login = $('#login').val();
				var pass = $('#pass').val();
				
				if (!login || login.length == 0)
				{
					$('#message_login').addClass('notification').addClass('note-error');
                    $('#message_login').show("blind", { direction: "vertical" }, 800);
                    $('#message_login').html('Você precisa digitar login/senha para entrar no sistema.');
                    $('#message_login').delay(2000).hide("blind", { direction: "vertical" }, 800);
				}
				else if (!pass || pass.length == 0)
				{
					$('#message_login').addClass('notification').addClass('note-error')
                    $('#message_login').show("blind", { direction: "vertical" }, 800);
                    $('#message_login').html('Você precisa digitar login/senha para entrar no sistema.');
                    $('#message_login').delay(2000).hide("blind", { direction: "vertical" }, 800);
				}
				else
				{
					var submitBt = $(this).find('button[type=submit]');
					submitBt.attr('disabled', true);
					
					// Target url
					var target = $(this).attr('action');
					if (!target || target == '')
					{
						// Page url without hash
						target = document.location.href.match(/^([^#]+)/)[1];
					}
					
					// Request
					var data = {
						login: login,
						pass: pass
					};
					var redirect = $('#redirect');
					if (redirect.length > 0)
					{
						data.redirect = redirect.val();
					}
					
					// Start timer
					var sendTimer = new Date().getTime();
					
					// Send
					$.ajax({
						url: target,
						dataType: 'json',
						type: 'POST',
						data: data,
						success: function(data, textStatus, XMLHttpRequest)
						{
							if (data.valid)
							{
								// Small timer to allow the 'cheking login' message to show when server is too fast
								var receiveTimer = new Date().getTime();
								if (receiveTimer-sendTimer < 500)
								{
									setTimeout(function()
									{
										document.location.href = data.redirect;
										
									}, 500-(receiveTimer-sendTimer));
								}
								else
								{
									document.location.href = data.redirect;
								}
							}
							else
							{
								// Message
								$('#message_login').addClass('notification').addClass('note-error')
			                    $('#message_login').show("blind", { direction: "vertical" }, 800);
			                    $('#message_login').html(data.error);
			                    $('#message_login').delay(2000).hide("blind", { direction: "vertical" }, 800);

								submitBt.attr('disabled', false);
							}
						},
						error: function(XMLHttpRequest, textStatus, errorThrown)
						{
							// Message
							$('#message_login').addClass('notification').addClass('note-error')
		                    $('#message_login').show("blind", { direction: "vertical" }, 800);
		                    $('#message_login').html('Falha ao tentar autenticar no servidor');
		                    $('#message_login').delay(2000).hide("blind", { direction: "vertical" }, 800);

							submitBt.attr('disabled', false);
						}
					});
					
					// Message
					//$('#message_login').addClass('notification').removeClass('note-error').addClass('note-info')
                    //$('#message_login').show("blind", { direction: "vertical" }, 100);
                    //$('#message_login').html('Autenticando...');
                    //$('#message_login').delay(800).hide("blind", { direction: "vertical" }, 100);
				}
			});
		});
  </script>

</body>
</html>
