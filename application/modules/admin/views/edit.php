<?php $this->load->view('includes/header');?>

<div class="main pagesize"> <!-- *** mainpage layout *** -->

  <!-- CONTENT BOX - DATATABLE -->
      <h1></h1>
      
      <div class="content-box">
      <div class="box-body">
        <div class="box-header clear">
          <h2>Editar Usuário</h2>
        </div>
        
        <div class="box-wrap clear">

          <div class="columns clear bt-space15">
    <?php 
          if(!empty($error))
          { ?>
            <div class="notification note-error">
				<a title="Close notification" class="close" href="#">close</a>
				<p><strong>Erro:</strong> <?php echo $error; ?></p>
			</div>
    
    <?php } ?>
    
    <?php 
          if(!empty($success))
          { ?>
            <div class="notification note-success">
				<a title="Close notification" class="close" href="#">close</a>
				<p><strong>Sucesso:</strong> <?php echo $success; ?></p>
			</div>
    
    <?php } ?>
            <form action="<?php echo base_url(); ?>admin/users/edit/save/" method="post" class="validate-form form bt-space15"  style="width: 500px;">

              <div class="col2-3" style="width:470px;">
                
                <div class="form-field clear">
                  <label for="textfield" class="form-label size-120 fl-space2">Foto: </label>
                  
                
                <?php
                  if(!empty($flap->facebook_id))
                    {
                        $urlAvatar = base_url().'userfiles/facebook/thumbnails/'.$flap->avatar;
                    }
                    else
                    {
                        if(empty($flap->avatar))
                        {
                            $urlAvatar = static_url().'admin/img/avatar.jpg';
                        }
                        else
                        {
                            $urlAvatar = base_url().'userfiles/thumbnails/'.$flap->avatar;
                        }
    
                    }
                  ?>
                  
                  <img src="<?php echo $urlAvatar; ?> " />
                  </div><!-- /.form-field -->
                      
                <div class="form-field clear">
                  <label for="textfield" class="form-label size-120 fl-space2">Nome:</label>
                  <input type="text" id="name" class="required text half fl-space2" name="name" value="<?php echo $flap->name; ?>" />
                </div><!-- /.form-field -->
                
                <div class="form-field clear">
                  <label for="textfield" class="form-label size-120 fl-space2">E-mail: </label>
                  <input type="text" id="email" class="required text half fl-space2" name="email" value="<?php echo $flap->email; ?>" />
                </div><!-- /.form-field -->

                <div class="form-field clear">
                  <label for="textfield" class="form-label size-120 fl-space2">Valor doado: </label>
                  <input type="text" id="pay_value" class="required text half fl-space2" name="pay_value" value="<?php echo $flap->pay_value; ?>" />
                </div><!-- /.form-field -->
                
                <div class="form-field clear">
                  <label for="textfield" class="form-label size-120 fl-space2">Posição: </label>
                  <input type="text" id="position" class="required text half fl-space2" name="position" value="<?php echo $flap->position; ?>" />
                </div><!-- /.form-field -->

                <div class="form-field clear">
                  <label for="textfield" class="form-label size-120 fl-space2">Status no site: </label>
                  <select id="status" class="fl-space2 required" name="status">
                    <?php 
                    if($flap->status == 0)
                        echo '<option value="0" selected="selected">Reprovado</option>';
                    else
                        echo '<option value="0" >Reprovado</option>';
                    
                     foreach($avStatus as $status)
                    {
                        if($flap->status == $status->id)
                            echo '<option value="'.$status->id.'" selected="selected">'.$status->name.'</option>';
                        else
                            echo '<option value="'.$status->id.'">'.$status->name.'</option>';
                    }  
                           
                    ?>
                  </select>
                </div><!-- /.form-field -->
                
                <div class="form-field clear">
                  <label for="textfield" class="form-label size-120 fl-space2">Status do pagamento: </label>
                  <select id="status_payment" class="fl-space2 required" name="status_payment">
                    <?php 
                     foreach($payStatus as $pstatus)
                    {
                        if($flap->status_payment == $pstatus->id)
                            echo '<option value="'.$pstatus->id.'" selected="selected">'.$pstatus->name.'</option>';
                        else
                            echo '<option value="'.$pstatus->id.'">'.$pstatus->name.'</option>';
                    }  
                           
                    ?>
                  </select>
                </div><!-- /.form-field -->
                
                
                <div class="form-field clear">
                  <label for="textfield" class="form-label size-120 fl-space2">&nbsp;</label>
                  <input type="submit" value="Enviar" class="button red fl-space"> 
                  <input type="hidden" name="user_id" value="<?php echo $flap->id; ?>" />
                </div><!-- /.form-field -->

            </div>

          </form>

          </div><!-- end of box-wrap -->

      </div> <!-- end of box-body -->
      </div> <!-- end of content-box -->
      </div>
</div>

  <?php $this->load->view('includes/footer');?>

  <link rel="stylesheet" href="<?php echo static_url(); ?>css/token-input.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo static_url(); ?>css/token-input-mac.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo static_url(); ?>css/token-input-facebook.css" type="text/css"/>
  <script type="text/javascript" src="<?php echo static_url(); ?>js/libs/jquery.tokeninput.js"></script>

  <script type="text/javascript">
  
  $(document).ready(function () {

      //InitMisc ();
      
      InitCufon (); 

      //InitEvents ();

      //InitBoxSlide ();

      //InitGraphs ();
        
      //InitNotifications ();
    
      //InitContentBoxes ();
    
      InitTables ();  
     
      //InitWYSIWYG ();
          
      //InitQuickEdit ();
      
      //InitMenuEffects ();
      
  });

  </script>

</body>
</html>