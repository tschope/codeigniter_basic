<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Admin_Controller {
    
   
   /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    private function _edit()
    {
        $rules = array(
                    array(
                            'field' => 'user_id',
                            'label' => '',
                            'rules' => 'required|xss_clean'
                         ),
                    array(
                            'field' => 'name',
                            'label' => '',
                            'rules' => 'xss_clean'
                         ),
                    array(
                            'field' => 'email',
                            'label' => '',
                            'rules' => 'xss_clean'
                         )
                    );
        
        $validate = $this->form_validation->validate_data($rules);
        $data_validate = $validate['data'];
        
        //Valida campos
        if ($validate['status'] == '1')
        {

            // Edit
            $u = new users_model();

            // Enter values into required fields
            $u->id                  = $data_validate['user_id'];
            $u->name                = $data_validate['name'];
            $u->email               = $data_validate['email'];

            // Save new user
            $u->save();

            $user_id = $u->id;

            $return['error']      = null;
            $return['success']    = 'Usuário editado com sucesso';
            $return['user_id']    = $user_id;

        }
        else
        {
            
            $return['error']      = $validate['errors'];
            $return['success']    = null;
            $return['user_id']    = null;
            
        }

        return $return;
    }
    
    /**
	 * Index Page for this controller.
	 *
	 */   
    public function index()
	{

        $this->load->view('index', $this->data);

	}
    
    /**
     * 
     */   
    public function edit()
    {
        
        //Se não for Admin - Out
        $url_param = $this->uri->segment(4);

        if($url_param == "save")
        {
            
            $save = $this->_edit();
            
            $this->data['error']   = $save['error'];
            $this->data['success'] = $save['success'];

            $u = new users_model();
            $data_edit = $u->where('id', $save['user_id'])->get();
            $this->data['user_data'] = $data_edit;

            $this->load->view('edit', $this->data);

        }
        else
        {

            if(is_int((int)$url_param))
            {
                $u = new users_model();
                $u->where('id', $url_param);
                $data_edit = $u->get();

                if(!empty($data_edit->id))
                {                    
                    $this->data['user_data'] = $data_edit;
                    $this->load->view('edit', $this->data);
                }
                else
                {
                    show_404();
                }

                
            }
            else
            {
                show_404();
            }

        }
        
    }

 
}


?>