<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Admin_Controller {
    
   
   /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        
    }

   /**
     * Privates Functions
     */
    
    
      
   

    /**
	 * Index Page for this controller.
	 *
	 */   
    public function index()
	{
       
        $this->load->view('login', $this->data);
        
	}
    
    /**
     * ****+-+-+-+-+-+-+-+-+-+-+-+-+*****
     *
     *
     *
     *      FUNCOES DE AUTENTICACAO
     *
     *
     *
     * ****+-+-+-+-+-+-+-+-+-+-+-+-+*****
     */

    /**
     * Registra a sessão do user e passa para home
     * Essa verificação é para ver se o usuario já
     * pertence ao sistema
     */
    private function _auth($username,$pass)
    {
        
        // Create user object
        $u = new users_model;
        
        $u->where(array('username' => $username, 'password' => $this->auth->_prep_password($pass)))->get();
        
        if ($u->exists())
        {
                $this->session->set_userdata('user_id',$u->id);
                $this->session->set_userdata('user_name',$u->name);
                $this->session->set_userdata('user_email',$u->email);

                $return = true;
        }
        else
        {
                $return = false;
        }
        
        return $return;
        
    }
    
    /**
     * Recebe o Post em AJAX de Login
     */
    public function loginPost()
    {

        //Recebe Dados
        $rules = array(
                    array(
                            'field' => 'username',
                            'label' => '',
                            'rules' => 'required|xss_clean'
                         ),
                    array(
                            'field' => 'pass',
                            'label' => '',
                            'rules' => 'required|xss_clean'
                         )
                    );
        
        $validate = $this->form_validation->validate_data($rules);
        $data_validate = $validate['data'];
        
        //Se obter todos os dados segue verificação
        if ($validate['status'] == '1')
        {
           
            $validate = $this->_auth($data_validate['username'],$data_validate['pass']);

            if($validate)
            {
                //Login OK, Vai para home
                $return = array(
                    'valid' => TRUE,
                    'redirect' => base_url().'admin/index'
                );
            }
            else
            {
                //Não faz parte do Sistema
                $return = array(
                    'valid' => FALSE,
                    'redirect' => '',
                    'error' => 'Você não faz parte do sistema',
                );
            }
        
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($return));

        }
    }

    /** logout * *
    * Destroy session login and redirect user to home
    * @access public
    * @return void
    */
    public function logout()
    {
        $this->session->sess_destroy();

        /**
        *  Redirect to default page 
        */
        header("Location: ".base_url()."admin/");
    }
 
}


?>