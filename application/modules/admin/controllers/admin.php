<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {
    
   
   /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $c = new config_model();
        $getC = $c->where('name','total_retalhos')->get();
        
        $this->data['maxFlaps'] = (int)$getC->value;
    }

   /**
     * Privates Functions
     */
    private function _getFlaps()
    {
        $return = array();
        
        $flaps = array();
        
        $u = new users_model();
        
        $flaps = $u->get();
        
        $totalFlaps = $this->data['maxFlaps'] - count($flaps->count());
        
        $this->session->set_userdata('totalFlaps',$totalFlaps);
        
        $return['totalFlaps'] = $totalFlaps;
        $return['flaps'] = $flaps;
        
        return $return;
    }
 

    /**
	 * Index Page for this controller.
	 *
	 */   
    public function index()
	{
       
        $getFlaps = $this->_getFlaps();
        
        $this->data['flaps'] = $getFlaps['flaps'];
        $this->data['totalFlaps'] = $getFlaps['totalFlaps'];
       
        $this->load->view('index', $this->data);
        
	}
 
}


?>