<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends Public_Controller
{
    /**
     * person 404
     * @return view 404 on themes page
     */
    public function error_404()
    {
        $this->data['totalFlaps'] = '0';
        
        $this->data['title_page'] = "Error 404 - Page not found";
        $this->output->set_status_header('404');
        $this->load->view('404', $this->data);
    }

}


/* End of file Error.php */
/* Location: ./application/controllers/Error.php */