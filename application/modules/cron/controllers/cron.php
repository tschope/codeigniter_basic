<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends Public_Controller {

   /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

    }

   /**
     * Privates Functions
     */
    private function _trataMoney($money)
    {
        $money = str_replace(",", ".", $money);
        return $money;
    }
    
      
   /**
    * Aqui dispara o email de notificação para um novo cadastro na plataforma 
    * 
    * @param $emails String or Array
    * @return boolean  
    */
    private function sendNotification($emails,$data)
    {
        
        $email_title = '[CobertorSocial]Relátorio';
        
        $this->data['dados'] = $data;
        
        $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
               );
         $this->load->library('email', $config);

         $this->email->from('cobertorsocial@cobertorsocial.com.br', utf8_encode('CobertorSocial'));
         $this->email->to($emails);
         
         //Carrega a view e coloca na view dados como email e etc;
         $body = $this->load->view("emails/relatorio", $this->data, TRUE);
          
         $this->email->subject($email_title);
         $this->email->message($body);

         if (!$this->email->send())
         {
            // Generate error
            return false;
         }
         else
         {
            return true;
         }
        
    }

    /**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */   
    public function index()
	{
         
        //Total de retalhos
        //Total de retalhos disponiveis
        //Total de Retalhos comprados
        //Total de retalhos Reservados
        //Total de Retalhos confirmados Pagamentos
        //Total de Dinheiro pendente
        //Total de dinheiro confirmado
        //Total de dinheiro 
        
        $c = new config_model();
        $getC = $c->where('name','total_retalhos')->get();
        
        $totalRetalhos = $getC->value;
        
        $query = $this->db->query(' SELECT
                                    COUNT(users.id) as total
                                    FROM
                                    users
                                    WHERE users.`status` = 1 OR users.`status` = 3');
        foreach ($query->result() as $row)
        {
           $totalRetalhosComprados = $row->total;
        }
        
        $query = $this->db->query(' SELECT
                                    COUNT(users.id) as total
                                    FROM
                                    users
                                    WHERE users.`status` = 2');
        foreach ($query->result() as $row)
        {
           $totalRetalhosReservados = $row->total;
        }
        
        $query = $this->db->query(' SELECT
                                    SUM(users.pay_value) as total
                                    FROM
                                    users
                                    WHERE users.`status` = 1 OR users.`status` = 3');
        foreach ($query->result() as $row)
        {
           $valorArrecadado = $row->total;
        }
        
        $query = $this->db->query(' SELECT
                                    SUM(users.pay_value) as total
                                    FROM
                                    users
                                    WHERE users.`status` = 2');
        foreach ($query->result() as $row)
        {
           $valorPendente = $row->total;
        }
        
        $totalQPodeSerArrecadado = $valorPendente+$valorArrecadado;
        
        $dadosEmail = array();
        $dadosEmail['TotalRetalhos'] = $totalRetalhos;
        $dadosEmail['TotalRetalhosDisponiveis'] = $totalRetalhos-($totalRetalhosComprados+$totalRetalhosReservados);
        $dadosEmail['totalRetalhosComprados'] = $totalRetalhosComprados;
        $dadosEmail['totalRetalhosReservados'] = $totalRetalhosReservados;
        $dadosEmail['valorArrecadado'] = $valorArrecadado;
        $dadosEmail['valorPendente'] = $valorPendente;
        $dadosEmail['totalQPodeSerArrecadado'] = $totalQPodeSerArrecadado;
        
        $emailDestiny = array('tschope@gmail.com','fabiodiasbh@gmail.com','rh@gruponewcomm.com.br','douglas.miguel@gmail.com');
        
        $this->sendNotification($emailDestiny, $dadosEmail);
		
	}
    

}


?>