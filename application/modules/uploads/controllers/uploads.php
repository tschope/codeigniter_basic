<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author tschope
 * @copyright 2012
 */

class Uploads extends Public_Controller {
    
    public $absolute_path;
    
    private function image($image_path, $width = 0, $height = 0) {
        
        //Get the Codeigniter object by reference
        $CI = & get_instance();
        
        //Alternative image if file was not found
        if (! file_exists($image_path))
            $image_path = 'images/file_not_found.jpg';
        
        //The new generated filename we want
        $fileinfo = pathinfo($image_path);
        $new_image_path = $fileinfo['dirname'] . '/thumbnails/' . $fileinfo['filename'] . '.' . $fileinfo['extension'];
        
        //The first time the image is requested
        //Or the original image is newer than our cache image
        if ((! file_exists($new_image_path)) || filemtime($new_image_path) < filemtime($image_path)) {
            $CI->load->library('image_lib');
           
            //The original sizes
            $original_size = getimagesize($image_path);
            $original_width = $original_size[0];
            $original_height = $original_size[1];
            $ratio = $original_width / $original_height;
           
            //The requested sizes
            $requested_width = $width;
            $requested_height = $height;
           
            //Initialising
            $new_width = 0;
            $new_height = 0;
           
            //Calculations
            if ($requested_width > $requested_height) {
                $new_width = $requested_width;
                $new_height = $new_width / $ratio;
                if ($requested_height == 0)
                    $requested_height = $new_height;
               
                if ($new_height < $requested_height) {
                    $new_height = $requested_height;
                    $new_width = $new_height * $ratio;
                }
           
            }
            else {
                $new_height = $requested_height;
                $new_width = $new_height * $ratio;
                if ($requested_width == 0)
                    $requested_width = $new_width;
               
                if ($new_width < $requested_width) {
                    $new_width = $requested_width;
                    $new_height = $new_width / $ratio;
                }
            }
           
            $new_width = ceil($new_width);
            $new_height = ceil($new_height);
           
            //Resizing
            $config = array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $image_path;
            $config['new_image'] = $new_image_path;
            $config['maintain_ratio'] = FALSE;
            $config['height'] = $new_height;
            $config['width'] = $new_width;
            $CI->image_lib->initialize($config);
            $CI->image_lib->resize();
            $CI->image_lib->clear();
           
            //Crop if both width and height are not zero
            if (($width != 0) && ($height != 0)) {
                $x_axis = floor(($new_width - $width) / 2);
                $y_axis = floor(($new_height - $height) / 2);
               
                //Cropping
                $config = array();
                $config['source_image'] = $new_image_path;
                $config['maintain_ratio'] = FALSE;
                $config['new_image'] = $new_image_path;
                $config['width'] = $width;
                $config['height'] = $height;
                $config['x_axis'] = $x_axis;
                $config['y_axis'] = $y_axis;
                $CI->image_lib->initialize($config);
                $CI->image_lib->crop();
                $CI->image_lib->clear();
            }
        }
        
        $filedetails = array();
        $filedetails['filename'] = $fileinfo['filename'] . '.' . $fileinfo['extension'];
        
        return $filedetails;
        
    }
    
    public function __construct()
	{
		parent::__construct();

	}

    public function index()
	{
      
        $this->data['title_page'] = "";
        $this->data['error'] = "";
        $this->load->view('index', $this->data);
	}
    
    public function do_upload()
	{
        
        if(!IS_AJAX){
            
            $config['upload_path'] = './userfiles/';

            //$config['allowed_types'] = 'jpg|gif|png|bmp|jpeg|tiff|tif|psd|pdf|ai|eps|ps|cdr|ps|svg|ppt|pptx|swf|3dm|max|obj|dwg|dxf|dgn|stl|otf|ttf|fnt|mp3|mid|midi|ra|wav|aif|aiff|aifc|ram|mpga|mpeg|mpg|mpe|mpga|qt|mov|avi|wmv|movie|rm|rv|mp4';
            $config['allowed_types'] = 'jpg|gif|png|jpeg';
 
            $this->load->library('upload', $config);
            
            if ($this->upload->do_upload('Filedata')) {
               //want to create thumbnail
                $image_data = $this->upload->data(); //get image data

                $filedetails = $this->image($image_data['full_path'], 80,80);          
                
                $this->output
                    ->set_content_type('application/json')
                    ->set_output($filedetails['filename']);
                
                //echo '<a id="remove_img" href="#" style="position:relative; top:0px; left:247px; width:10px; height:10px; display:block;" ><img style="border:none;"  src="'.static_url().'themes/default/img/cross-circle.png" /></a>'."\n";
                //echo '<img src="'.base_url().'userfiles/thumbnails/'.$filedetails['filename'].'" />'."\n";
                
                //echo '<input type="hidden" name="avatar" id="avatar" value="'.$filedetails['filename'].'" />'."\n";

                
            } else {
                
                echo $this->upload->display_errors();
            }
            
            
        }
        else
        {
            echo 'is not ajax';
        }   

    }
	

    public function delPhoto()//gets the job done but you might want to add error checking and security
	{

        $rules = array(
                    array(
                            'field' => 'photo',
                            'label' => '',
                            'rules' => 'required|xss_clean'
                         )
                    );
        
        $validate = $this->form_validation->validate_data($rules);
        $data_validate = $validate['data'];
        
        //Valida campos
        if ($validate['status'] == '1')
        {

            $file = $data_validate['photo'];

            if(file_exists('./userfiles/'.$file))
            {
                unlink('./userfiles/'.$file);
            }
            
            if(file_exists('./userfiles/thumbnails/'.$file))
            {
                unlink('./userfiles/thumbnails/'.$file);
            }
            echo 'true';
            

        }
        else
        {
            echo $validate['errors'];
        } 

    }
 
 }

/* End of file Upload.php */
/* Location: ./application/modules/upload/upload.php */
?>