<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Arquivo para as configurações do PagSeguro
 * 
 * @author tschope
 * @copyright 2012
 */ 

$pgs_config = array();
$pgs_config['email'] = 'EMAILPAGSEGURO';
$pgs_config['TOKEN'] = 'TOKEN';


/* End of file pagseguro.php */
/* Location: ./system/application/config/pagseguro.php */

?>