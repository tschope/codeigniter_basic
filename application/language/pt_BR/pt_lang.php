<?php

/**
 * @author Cliente
 * @copyright 2011
 * 
 * Arquivo com os textos dinamicos de TAM
 * 
 */

$lang['title_form']        = "<h1>FORMULÁRIO DE SOLICITAÇÃO</h1>";

//$lang['msg_header'] = "<p><span>Prezado Cliente,<span></p>
//<p>Este formul&aacute;rio &eacute; destinado &agrave; solicita&ccedil;&atilde;o do Traslado Cortesia para passageiros em Primeira Classe 
//e Classe Executiva (classe C e D) com embarque ou desembarque nos aeroportos internacionais operados pela TAM.</p>
//<p>O Traslado Cortesia em carro privatico n&atilde;o est&aacute; dispon&iacute;vel para cidade no Brasil.</p>
//<p>Pedimos, por gentileza, o preenchimento do formul&aacute;rio abaixo para a solicita&ccedil;&atilde;o do servi&ccedil;o. 
//Sua solicita&ccedil;&atilde;o ser&aacute; analisada e enviaremos a confirma&ccedil;&atilde;o do traslado cortesia atrav&eacute;s do contato de e-mail informado abaixo</p>";
//

$lang['msg_header']         = "<p><span>Prezado Cliente,<span></p>";
$lang['msg_header']        .= "<p>Pedimos, por gentileza, o preenchimento do formul&aacute;rio abaixo para a solicita&ccedil;&atilde;o do servi&ccedil;o."; 
$lang['msg_header']        .= "Sua solicita&ccedil;&atilde;o ser&aacute; analisada e enviaremos a confirma&ccedil;&atilde;o".
$lang['msg_header']        .= "do traslado cortesia atrav&eacute;s do contato de e-mail informado abaixo:</p>";


$lang['names']              = "Nome do(s) passageiro(s)<span>*</span>";
$lang['num_pass']           = "Quantidade total de Passageiro(s)<span>*</span>";
$lang['localizador']        = "Localizador da reserva";
$lang['localizador_pass']   = "Localizador acompanhante(s)";
$lang['n_voo']              = "N&uacute;mero do voo";
$lang['voo_date']           = "N&uacute;mero do voo e data de embarque<span>*</span>";
$lang['ida_volta']          = "Ida e Volta<span>*</span>";

$lang['same_go']            = "Endereço de volta é o mesmo que o de ida?";

$lang['address']            = "Endere&ccedil;o completo<span>*</span>";

$lang['city']               = "Cidade";
$lang['state']              = "Estado";
$lang['country']            = "Pa&iacute;s";
$lang['cep']                = "CEP<span>*</span>";

$lang['tel_fix']            = "Telefone Fixo (Hotel/Resid&ecirc;ncia/Escrit&oacute;rio)<span>*</span>";
$lang['tel_cel']            = "Telefone celular habilitado no exterior";
$lang['tel_br']             = "Telefone de contato no Brasil<span>*</span>";

$lang['num_bag']            = "Quantidade total de bagagens<span>*</span>";
$lang['hour']               = "Hora da Sa&iacute;da";
$lang['minutes']            = "Minuto de Sa&iacute;da";
$lang['hour_left_ext']      = "Hora/Data programada para o servi&ccedil;o";
$lang['kids_abord']         = "Quantidade de crian&ccedil;a(s) ou beb&ecirc;(s)";

$lang['voo_date_ext']       = "Data da Sa&iacute;da";

$lang['kids_abord_msg']     = "Favor informar nome e idade";
$lang['kids_abord_name']    = "Nome";
$lang['kids_abord_years']   = "Idade";

$lang['email']              = "E-mail<span>*</span>";
$lang['observations']       = "Observa&ccedil;&otilde;es";

$lang['404_title']          = "<h1>404 - P&aacute;gina n&atilde;o encontrada!<br />&nbsp;</h1>";
$lang['404_msg']            = "<p>P&aacute;gina n&atilde;o encontrada! Tente outro endere&ccedil;o</p>";

$lang['success_msg']        = "<p>Sua solicita&ccedil;&atilde;o foi enviada com sucesso e ser&aacute; respondida em at&eacute; 24 horas.</p>";
$lang['error_msg']          = '<p>Sua solicita&ccedil;&atilde;o n&atilde;o foi processada. <a href="javascript:void(0)" class="return_form">Clique aqui</a> para voltar ao fomul&aacute;rio.</p>';


$lang['bt_send']            = "Enviar";



?>
