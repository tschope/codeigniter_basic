<?php

$lang['required']             = "O campo %s &eacute; obrigat&oacute;rio.";
$lang['isset']                = "O campo %s deve ter um valor.";
$lang['valid_email']          = "O campo %s deve conter um enderešo de e-mail v&aacute;lido.";
$lang['valid_emails']         = "O campo %s deve conter todos os enderešos de e-mail v&aacute;lidos.";
$lang['valid_url']            = "O campo %s deve conter um URL v&aacute;lido.";
$lang['valid_ip']             = "O campo %s deve conter um IP v&aacute;lido.";
$lang['min_length']           = "O campo %s deve ter no m&iacute;nimo %s caracteres.";
$lang['max_length']           = "O campo %s deve ter no m&aacute;ximo %s caracteres.";
$lang['exact_length']         = "O campo %s deve ter exatamente %s caracteres.";
$lang['alpha']                = "O campo %s pode conter apenas caracteres alfab&eacute;ticos.";
$lang['alpha_numeric']        = "O campo %s pode conter apenas caracteres alfa-num&eacute;ricos.";
$lang['alpha_dash']           = "O campo %s pode conter apenas caracteres alfa-num&eacute;ricos, underscores e h&iacute;fens.";
$lang['numeric']              = "O campo %s deve conter apenas n&uacute;meros.";
$lang['is_numeric']           = "O campo %s deve conter apenas caracteres num&eacute;ricos.";
$lang['integer']              = "O campo %s deve conter um inteiro.";
$lang['matches']              = "O campo %s n&atilde;o coincide com o campo %s.";
$lang['is_natural']           = "O campo %s deve conter apenas n&uacute;meros positivos.";
$lang['is_natural_no_zero']   = "O campo %s deve conter um n&uacute;mero maior que zero.";

/* adicionado por Ygor Amaral */
$lang['username']                         = "Usu&aacute;rio";
$lang['password']                         = "Senha";
$lang['_check_login']             = "O nome de usu&aacute;rio e senha digitados s&atilde;o incorretos.";

/* End of file form_validation_lang.php */
/* Location: ./system/language/pt-br/form_validation_lang.php */