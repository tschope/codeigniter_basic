<?php

/**
 * @author tschope
 * @copyright 2012
 */

/**
 * Get image properties
 *
 * A helper function that gets info about the file
 * Require GD
 *
 * @access    public
 * @param    string
 * @return    mixed
 */            
if ( ! function_exists('get_image_properties'))
{
    function get_image_properties($path = '', $return = FALSE)
    {

        if ($path == '')
            $path = $this->full_src_path;
                
        if ( ! file_exists($path))
        {
            $this->set_error('imglib_invalid_path');        
            return FALSE;                
        }
        
        $vals = @getimagesize($path);
        
        $types = array(1 => 'gif', 2 => 'jpeg', 3 => 'png');
        
        $mime = (isset($types[$vals['2']])) ? 'image/'.$types[$vals['2']] : 'image/jpg';
                
        if ($return == TRUE)
        {
            $v['width']            = $vals['0'];
            $v['height']        = $vals['1'];
            $v['image_type']    = $vals['2'];
            $v['size_str']        = $vals['3'];
            $v['mime_type']        = $mime;
            
            return $v;
        }
        
        $this->orig_width    = $vals['0'];
        $this->orig_height    = $vals['1'];
        $this->image_type    = $vals['2'];
        $this->size_str        = $vals['3'];
        $this->mime_type    = $mime;
        
        return TRUE;
    }
}

/* 
  filename without extension 
  ex: file_core_name('toto.jpg') -> 'toto'
*/
if ( ! function_exists('file_core_name'))
{
	function file_core_name($file_name)
	{
		$exploded = explode('.', $file_name);
 
		// if no extension
		if (count($exploded) == 1)
		{
			return $file_name;
		}
 
		// remove extension
		array_pop($exploded);
 
		return implode('.', $exploded);
	}
}
 
/* 
  file extension 
  ex: file_extension('toto.jpg') -> 'jpg'
*/
 
if ( ! function_exists('file_extension'))
{
	function file_extension($path)
	{
		$extension = substr(strrchr($path, '.'), 1);
		return $extension;
	}
}
 
/* 
  file size 
  ex: file_size('toto.jpg') -> '3.3 MB'
*/
if ( ! function_exists('file_size'))
{
	function file_size($path)
	{
		$num = filesize($path);
 
		// code from byte_format()
		$CI =& get_instance();
		$CI->lang->load('number');
 
		$decimals = 1;
 
		if ($num >= 1000000000000) 
		{
			$num = round($num / 1099511627776, 1);
			$unit = $CI->lang->line('terabyte_abbr');
		}
		elseif ($num >= 1000000000) 
		{
			$num = round($num / 1073741824, 1);
			$unit = $CI->lang->line('gigabyte_abbr');
		}
		elseif ($num >= 1000000) 
		{
			$num = round($num / 1048576, 1);
			$unit = $CI->lang->line('megabyte_abbr');
		}
		elseif ($num >= 1000) 
		{
			$decimals = 0; // decimals are not meaningful enough at this point
 
			$num = round($num / 1024, 1);
			$unit = $CI->lang->line('kilobyte_abbr');
		}
		else
		{
			$unit = $CI->lang->line('bytes');
			return number_format($num).' '.$unit;
		}
 
		$str = number_format($num, $decimals).' '.$unit;
 
		$str = str_replace(' ', '&nbsp;', $str);
		return $str;
	}
}

if ( ! function_exists('myTruncate'))
{
    function myTruncate($string, $limit, $break=" ", $pad="{...}") 
    { 
        // return with no change if string is shorter than $limit 
        if(strlen($string) <= $limit) return $string; 
        $string = substr($string, 0, $limit); 
        
        if(false !== ($breakpoint = strrpos($string, $break))) 
        { 
            $string = substr($string, 0, $breakpoint); 
        } 
        return $string . $pad; 
    }
}

?>