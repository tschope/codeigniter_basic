<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * Função para carregar a pasta absoluta
 * dos arquivos staticos
 * 
 */
if ( ! function_exists('static_url'))
{
	function static_url()
	{
		$CI =& get_instance();
		$CI->load->helper('url');
		return base_url().'static/';
	}
}
/**
 * 
 * Função para carregar a pasta absoluta
 * dos arquivos staticos de admin
 * 
 */
if ( ! function_exists('ad_static_url'))
{
	function ad_static_url()
	{
		$CI =& get_instance();
		$CI->load->helper('url');
		return base_url().'static/admin/';
	}
}
/**
 * 
 * Função para carregar a pasta absoluta
 * do servidor, para facilitar uploads
 * 
 */
if ( ! function_exists('absolute_path'))
{
	function absolute_path()
	{
		return str_replace('system/','', BASEPATH);
	}
}

if ( ! function_exists('json_encode')) {
    function json_encode($data) {
        switch ($type = gettype($data)) {
            case 'NULL':
                return 'null';
            case 'boolean':
                return ($data ? 'true' : 'false');
            case 'integer':
            case 'double':
            case 'float':
                return $data;
            case 'string':
                return '"' . addslashes($data) . '"';
            case 'object':
                $data = get_object_vars($data);
            case 'array':
                $output_index_count = 0;
                $output_indexed = array();
                $output_associative = array();
                foreach ($data as $key => $value) {
                    $output_indexed[] = json_encode($value);
                    $output_associative[] = json_encode($key) . ':' . json_encode($value);
                    if ($output_index_count !== NULL && $output_index_count++ !== $key) {
                        $output_index_count = NULL;
                    }
                }
                if ($output_index_count !== NULL) {
                    return '[' . implode(',', $output_indexed) . ']';
                } else {
                    return '{' . implode(',', $output_associative) . '}';
                }
            default:
                return ''; // Not supported
        }
    }
}


if( ! function_exists('url_title'))
{

    /**
     * Create URL Title
     *
     * Takes a "title" string as input and creates a
     * human-friendly URL string with either a dash
     * or an underscore as the word separator.
     *
     * @access    public
     * @param     string    the string
     * @param     string    the separator: dash, or underscore
     * @return    string
     * 
     * @co-author Rodrigo Tschope
     * 
     * Melhora na parte de Acentuação, ou seja, quando tem acento
     * ao invez de eliminar a letra o codigo substitui pela correta
     * 
    */

    function url_title($str, $separator = 'dash')
    {
        if ($separator == 'dash')
        {
            $search     = '_';
            $replace    = '-';
        }
        else
        {
            $search     = '-';
            $replace    = '_';
        }

        $str = convert_accented_characters($str);
        
        $trans = array(
                        $search                   => $replace,
                        "\s+"                     => $replace,
                        "[^a-z0-9".$replace."]"   => '',
                        $replace."+"              => $replace,
                        $replace."$"              => '',
                        "^".$replace              => ''
                       );

        $str = strip_tags(strtolower($str));

        foreach ($trans as $key => $val)
        {
            $str = preg_replace("#".$key."#", $val, $str);
        }
        return trim(stripslashes($str));
    }

}
if( ! function_exists('convert_accented_characters'))
{

    /**
     *
     * Transforma letras com acento em letras em
     * minusculo sem acento
     * 
     * @access    public
     * @param     string    the string
     * @param     string    type encode
     * @return    string
     * 
     * @author Rodrigo Tschope
     * 
     * 
    */
    
    function convert_accented_characters($var, $enc = 'UTF-8')
    {
    
    	$acentos = array(
    	           'A' =>'/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
    	           'a' =>'/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
    	           'C' => '/&Ccedil;/',
    	           'c' => '/&ccedil;/',
    	           'E' =>'/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
    	           'e' =>'/&egrave;|&eacute;|&ecirc;|&euml;/',
    	           'I' =>'/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
    	           'i' =>'/&igrave;|&iacute;|&icirc;|&iuml;/',
    	           'N' => '/&Ntilde;/',
    	           'n' => '/&ntilde;/',
    	           'O' =>'/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
    	           'o' =>'/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
    	           'U' =>'/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
    	           'u' =>'/&ugrave;|&uacute;|&ucirc;|&uuml;/',
    	           'Y' => '/&Yacute;/',
    	           'y' => '/&yacute;|&yuml;/',
    	           'a.' => '/&ordf;/',
    	           'o.' => '/&ordm;/'
    	               );
         $var = preg_replace($acentos,
    			array_keys($acentos),
    			htmlentities($var, ENT_NOQUOTES, $enc)
    			);
    
       return strtolower($var);
    
    }

}
