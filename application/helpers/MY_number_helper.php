<?php

/**
 * @author tschope
 * @copyright 2012
 */



if ( ! function_exists('convert_kilobytes_to_bytes'))
{
	function convert_kilobytes_to_bytes($number_kilobytes)
	{
        $CI =& get_instance();
		$CI->lang->load('number');
        
        $kilobytes = str_replace(',','.', $number_kilobytes);
        return $kilobytes*1024;
	}
}

?>