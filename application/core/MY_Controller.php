<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Base Controller
 * 
 */ 
class MY_Controller extends CI_Controller {

    //All Data
    var $data = null;
    var $dataDebugging = null;
    
    function __construct()
    {
        parent::__construct();
        // Load shared resources here or in autoload.php
        //Load something(Librarys, models or others files) based in ENVIRONMENT - Define in index.php
		if (ENVIRONMENT == 'development')
		{
			$this->load->library('firephp');
            $this->firephp->setEnabled(TRUE);
            $this->dataDebugging['FirePHP'] = true;
		}
		elseif (ENVIRONMENT == 'testing')
		{
			$this->load->library('firephp');
            $this->firephp->setEnabled(TRUE);
            $this->dataDebugging['FirePHP'] = true;
		}
		elseif (ENVIRONMENT == 'production')
		{
            $this->load->library('firephp_fake');
			$this->firephp =& $this->firephp_fake;
            $this->dataDebugging['FirePHP'] = false;
		}
        
        $this->data['title_page'] = '';
        
    }
}

/**
 * Admin Controller
 * 
 */ 
class Admin_Controller extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        
        //Title for all pages
        $this->data['title_page'] = 'Admin';
        
        /*
     	 * Check if user is logged
     	 */
        $this->load->library('auth');
     	$logged = $this->auth->verify_auth();

		if (!empty($logged))
		{
			$this->data['logged'] = true;
			$this->data['email'] = $logged['user_email'];
			$this->data['user_data'] = $logged;


		}
        else
        {
            
            $this->data['logged'] = false;
			$this->data['email'] = null;
			$this->data['user_data'] = null;

            $segment0 = $this->uri->segment(0);
            $segment1 = $this->uri->segment(1);
            $segment2 = $this->uri->segment(2);
            
            if($segment1 == 'admin')
            {
                if($segment2 == 'loginPost')
                {
                    //do noting
                }
                else if($segment2 != 'login')
                {
                    redirect(base_url()."admin/login/", 'refresh');
                }
                
            }
            else
            {
                redirect(base_url()."admin/login/", 'refresh');
            }
            
        }
        
    }
}

/**
 * Default Site Controller
 * 
 */ 
class Public_Controller extends MY_Controller {

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();	
        //Title for all pages
        $this->data['title_page'] = 'Site';	
    
    }
    
}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */