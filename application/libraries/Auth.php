<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Auth Class
 *
 * @package        	CodeIgniter
 * @subpackage    	Libraries
 * @category    	Libraries
 * @author        	Tiago M. Vanderlei
 * @link			http://www.kynho.com
 */
class Auth
{
	private $_ci;
	
	public function __construct()
	{
		$this->_ci =& get_instance();
	}
	
	/** Set last url **
    * sets the last url accessed by the user
    * @access public
    * @return array
    */
    public function set_last_url()
    {
    	$this->_ci->session->set_userdata('last_url',current_url());
	}
	
	/** Verify auth **
    * checks if user are logged in
    * @access public
    * @return array
    */
    public function verify_auth()
    {
    	$user_id = $this->_ci->session->userdata('user_id');
        
		$return = array();
		
		if ( ! empty($user_id))
		{
			$return['user_id']       = $user_id;
            $return['user_name']     = $this->_ci->session->userdata('user_name');
			$return['user_email']    = $this->_ci->session->userdata('user_email');
		}
		return $return;
    }
	
	/** auth redirect **
    * redirect user
    * @access public
    * @return void
    */
    public function auth_redirect($uri = NULL)
    {
		if ($uri)
		{
			/**
			*  access the signup page
			*/
			redirect(base_url().$uri,"refresh");
		}
		
		$last_url = $this->_ci->session->userdata('last_url');
		
    	if ( ! empty($last_url))
		{
			/**
			*  access the last page 
			*/
			redirect($last_url,"refresh");
		}
		else
		{
			/**
			*  Redirect to default page 
			*/
			redirect(base_url(),"refresh");	
		}
    }
	
	/** auth redirect **
    * get redirect url
    * @access public
    * @return void
    */
    public function auth_get_redirect_url()
    {
		$last_url = $this->_ci->session->userdata('last_url');
		
    	if ( ! empty($last_url))
		{
			/**
			*  access the last page 
			*/
			return $last_url;
		}
		else
		{
			/**
			*  Redirect to default page 
			*/
			return base_url();
		}
    }
    
    public function _prep_password($password)
    {
         return sha1($password.$this->_ci->config->item('encryption_key'));
    }
}


/* End of file Auth.php */
/* Location: ./application/libraries/Auth.php */