<?php

/*

Classe de Tratamento de Retorno do Pagseguro

*/

class Pagseguronpi {
	
	private $timeout = 20; // Timeout em segundos

    private $_ci;
	
	public function __construct()
	{
		$this->_ci =& get_instance();
	}
    
	public function notificationPost($token) {
		$postdata = 'Comando=validar&Token='.$token;
		
		foreach ($_POST as $key => $value) {
			if('string'!==gettype($value)) $_POST[$key]='';
			$value=urlencode(stripslashes(mb_convert_encoding($value, 'ISO-8859-1', 'ISO-8859-1,ASCII,UTF-8')));
			$key=mb_convert_encoding($key, 'ISO-8859-1', 'ISO-8859-1,ASCII,UTF-8');
			
			$valued    = $this->clearStr($value);
			$postdata .= "&$key=$valued";
		}
		return $this->verify($postdata);
	}
	

	
	private function clearStr($str) {
		if (!get_magic_quotes_gpc()) {
			$str = addslashes($str);
		}
		return $str;
	}
	
	private function verify($data) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://pagseguro.uol.com.br/pagseguro-ws/checkout/NPI.jhtml");
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$result = trim(curl_exec($curl));
		curl_close($curl);
		return $result;
	}

}

?>