<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facebook_model extends CI_Model {
 
    public function __construct()
    {
        parent::__construct();
 
        //Load the Securimage config file
        include(APPPATH.'config/facebook'.EXT);
 
        $this->load->library('Facebook', $fb_config);
 
        $user = $this->facebook->getUser();
        
        // We may or may not have this data based on whether the user is logged in.
        //
        // If we have a $user id here, it means we know the user is logged into
        // Facebook, but we don't know if the access token is valid. An access
        // token is invalid if the user logged out of Facebook.
        $profile = null;
        if($user)
        {
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                $profile = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        }
 
        $fb_data = array(
                        'me' => $profile,
                        'uid' => $user,
                        'loginUrl' => $this->facebook->getLoginUrl(),
                        'logoutUrl' => $this->facebook->getLogoutUrl(),
                        'appId' => $fb_config['appId'],
                        'secret' => $fb_config['secret'],
                    );
        
        $this->session->set_userdata('fb_data', $fb_data);
    }
}