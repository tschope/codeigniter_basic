<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class config_model extends DataMapper {

    public $model = "config_models";
    public $table = "configs";
    public $default_order_by = array("id");
    public $has_one = array();
    public $has_many = array();
    public $validation = array();
    
    public function __construct($id = NULL) {
     parent::__construct($id);
    }


}