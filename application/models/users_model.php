<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class users_model extends DataMapper {

    public $model = "users_models";
    public $table = "users";
    public $default_order_by = array("id");
    public $has_one = array();
    public $has_many = array();
    public $validation = array();
    
    public function __construct($id = NULL) {
     parent::__construct($id);
    }


}